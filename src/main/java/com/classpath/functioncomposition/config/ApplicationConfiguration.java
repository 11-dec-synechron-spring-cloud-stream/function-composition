package com.classpath.functioncomposition.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public Function<String, String> preProcess(){
        return (input) -> {
            System.out.println(" Pre Processing the data");
            return input;
        };
    }

    @Bean
    public Function<String, String> process(){
        return (value) -> {
            System.out.println("Processing the data");
            return value.toUpperCase();
        };
    }

    @Bean
    public Function<String, String> postProcess(){
        return (val) -> {
            System.out.println("Post Processing the data");
            return val;
        };
    }

    @Bean
    public Function<String, String> toUpperCase(){
        return String::toUpperCase;
    }

}